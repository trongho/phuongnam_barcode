package com.symbol.barcodesample1.Util;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    public final static String DBNAME = "BARCODE_MANAGER";
    public final static int DBVERSION = 1;

    public DBHelper(Context context) {
        super(context, DBNAME, null, DBVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql;
        //creat session table
        sql = "CREATE TABLE SESSION(" +
                "ID_SESSION INTEGER PRIMARY KEY AUTOINCREMENT," +
                "SESSION_NUMBER INTEGER NOT NULL," +
                "DEVICE_NUMBER TEXT NOT NULL,"+
                "SUMSKU INTEGER NOT NULL,"+
                "CREATE_DATE DATE NOT NULL,"+
                "SYNC_STATUS TEXT NOT NULL)";
        sqLiteDatabase.execSQL(sql);
        //creat book table
        sql = "CREATE TABLE BOOK(" +
                "SKU TEXT PRIMARY KEY)";
        sqLiteDatabase.execSQL(sql);
        //creat session detail table
        sql = "CREATE TABLE SESSION_DETAIL(" +
                "ID_SESSION INTEGER NOT NULL REFERENCES SESSION(ID_SESSION)," +
                "SKU TEXT NOT NULL REFERENCES BOOK(SKU) ,"+
                "QUANTITY INTEGER NOT NULL)";
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql;
        sql = "DROP TABLE IF EXISTS SESSION";
        sqLiteDatabase.execSQL(sql);
        sql = "DROP TABLE IF EXISTS BOOK";
        sqLiteDatabase.execSQL(sql);
        sql = "DROP TABLE IF EXISTS SESSION_DETAIL";
        sqLiteDatabase.execSQL(sql);
        onCreate(sqLiteDatabase);
    }
}
