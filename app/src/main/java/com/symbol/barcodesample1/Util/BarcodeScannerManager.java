package com.symbol.barcodesample1.Util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BarcodeScannerManager implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private String statusString = "";
    private TextView textViewStatus = null;
    private ArrayList<String> listData = null;
    private List<ScannerInfo> deviceList = null;
    private boolean bContinuousMode = false;
    private boolean bSoftTriggerSelected = false;
    private boolean bExtScannerDisconnected = false;
    private final Object lock = new Object();

    public BarcodeScannerManager(Context context,ArrayList<String> listData,TextView textViewStatus) {
        InitializeEMDK(context);
        this.listData=listData;
        this.textViewStatus = textViewStatus;
    }

    void InitializeEMDK(Context context) {
        EMDKResults results = EMDKManager.getEMDKManager(context, this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            statusString = "Status: " + "EMDKManager object request failed!";
            new AsyncStatusUpdate().execute(statusString);
            return;
        }
    }

    void InitializeBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(EMDKManager.FEATURE_TYPE.BARCODE);
        }
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        textViewStatus.setText("Status: " + "EMDK open success!");
        this.emdkManager = emdkManager;
        InitializeBarcodeManager();
        enumerateScannerDevices();
        initScanner();
    }

    private void enumerateScannerDevices() {
        deviceList = barcodeManager.getSupportedDevicesInfo();
    }

    @Override
    public void onClosed() {
        if (emdkManager != null) {

            // Remove connection listener
            if (barcodeManager != null) {
                barcodeManager.removeConnectionListener(this);
                barcodeManager = null;
            }

            // Release all the resources
            emdkManager.release();
            emdkManager = null;
        }
        textViewStatus.setText("Status: " + "EMDK closed unexpectedly! Please close and restart the application.");
    }

    public void enable() {

    }

    public void disable() {

    }

    @Override
    public void onStatus(StatusData statusData) {
        StatusData.ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
                statusString = statusData.getFriendlyName() + " is enabled and idle...";
                new AsyncStatusUpdate().execute(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = Scanner.TriggerType.SOFT_ONCE;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        new AsyncStatusUpdate().execute(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = Scanner.TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = Scanner.TriggerType.HARD;
                }
                // set decoders
                setDecoders();
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                        new AsyncStatusUpdate().execute(e.getMessage());
                    }
                }
                break;
            case WAITING:
                statusString = "Scanner is waiting for trigger press...";
                new AsyncStatusUpdate().execute(statusString);
                break;
            case SCANNING:
                statusString = "Scanning...";
                new AsyncStatusUpdate().execute(statusString);
                break;
            case DISABLED:
                statusString = statusData.getFriendlyName() + " is disabled.";
                new AsyncStatusUpdate().execute(statusString);
                break;
            case ERROR:
                statusString = "An error has occurred.";
                new AsyncStatusUpdate().execute(statusString);
                break;
            default:
                break;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, BarcodeManager.ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    bSoftTriggerSelected = false;
                    synchronized (lock) {
                        initScanner();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
            status = scannerNameExtScanner + ":" + statusExtScanner;
            new AsyncStatusUpdate().execute(status);
        } else {
            bExtScannerDisconnected = false;
            status = statusString + " " + scannerNameExtScanner + ":" + statusExtScanner;
            new AsyncStatusUpdate().execute(status);
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanDataCollection.ScanData> scanData = scanDataCollection.getScanData();
            for (ScanDataCollection.ScanData data : scanData) {
                String dataString = data.getData();
                new AsyncDataUpdate().execute(dataString);
            }
        }
    }

    private void initScanner() {
        deviceList = barcodeManager.getSupportedDevicesInfo();
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
                textViewStatus.setText("Status: " + "Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }

            if (scanner != null) {

                scanner.addDataListener(this);
                scanner.addStatusListener(this);

                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    textViewStatus.setText("Status: " + e.getMessage());
                    deInitScanner();
                }
            } else {
                textViewStatus.setText("Status: " + "Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.cancelRead();
                scanner.disable();

            } catch (ScannerException e) {
                textViewStatus.setText("Status: " + e.getMessage());
            }
            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
                textViewStatus.setText("Status: " + e.getMessage());
            }
            try {
                scanner.release();
            } catch (ScannerException e) {

                textViewStatus.setText("Status: " + e.getMessage());
            }
            scanner = null;
        }
    }

    private void setDecoders() {
        if (scanner == null) {
            initScanner();
        }
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.decoderParams.ean8.enabled = true;
                config.decoderParams.ean13.enabled = true;
                config.decoderParams.code39.enabled = true;
                config.decoderParams.code128.enabled = true;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                textViewStatus.setText("Status: " + e.getMessage());
            }
        }
    }

    private void setTrigger() {

        if (scanner == null) {
            initScanner();
        }

        if (scanner != null) {
            if(bSoftTriggerSelected==true){
                scanner.triggerType = Scanner.TriggerType.HARD;
            }else{
                scanner.triggerType = Scanner.TriggerType.SOFT_ALWAYS;
            }
        }
    }


    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                    new AsyncStatusUpdate().execute(e.getMessage());
                }
            }
        }
    }

    public void startScan(CheckBox checkBoxContinous) {
        if (scanner == null) {
            initScanner();
        }
        if (scanner != null) {
            try {
                if (scanner.isReadPending()) {
                    // Cancel the pending read.
                    scanner.cancelRead();
                }
                // Submit a new read.
                scanner.triggerType = Scanner.TriggerType.SOFT_ONCE;
                scanner.read();
                if (checkBoxContinous.isChecked())
                    bContinuousMode = true;
                else
                    bContinuousMode = false;
            } catch (ScannerException e) {
                textViewStatus.setText("Status: " + e.getMessage());
            }
        }
    }


    public void stopScan() {
        if (scanner != null) {
            try {
                // Reset continuous flag
                bContinuousMode = false;
                // Cancel the pending read.
                scanner.cancelRead();
            } catch (ScannerException e) {
                textViewStatus.setText("Status: " + e.getMessage());
            }
        }
    }


    public ArrayList<String> onScanReceiver() {
        return listData;
    }

    public void onDestroy() {
        deInitScanner();
        // Remove connection listener
        if (barcodeManager != null) {
            barcodeManager.removeConnectionListener(this);
            barcodeManager = null;
        }
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    public void onResume() {
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            InitializeBarcodeManager();
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            setTrigger();
            setDecoders();
        }
    }

    public void onPause() {
        deInitScanner();
        deInitBarcodeManager();
    }


    private class AsyncDataUpdate extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            return params[0];
        }

        protected void onPostExecute(String result) {
            listData.add(result);
            }
        }

    private class AsyncStatusUpdate extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            return params[0];
        }

        @Override
        protected void onPostExecute(String result) {
            textViewStatus.setText("Status: " + result);
        }
    }

    interface BarcodeScanCallback {
        public void handle(String scannedBarcode);
    }

}
