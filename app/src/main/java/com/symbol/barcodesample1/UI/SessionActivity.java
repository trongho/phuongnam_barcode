package com.symbol.barcodesample1.UI;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.symbol.barcodesample1.R;
import com.symbol.barcodesample1.Model.Session;
import com.symbol.barcodesample1.DAO.SessionDAO;
import com.symbol.barcodesample1.Model.SessionDetail;
import com.symbol.barcodesample1.DAO.SessionDetailDAO;
import com.symbol.barcodesample1.Adapter.SessionAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;

public class SessionActivity extends AppCompatActivity {
    private Spinner spYear = null;
    private Spinner spMonth = null;
    private Spinner spDevice=null;
    RecyclerView rvSession;
    SessionDAO sessionDAO;
    Session session;
    SessionAdapter sessionAdapter;
    ArrayList<Session> list;
    SessionDetailDAO sessionDetailDAO;
    SessionDetail sessionDetail;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    int curentYear = 2021;
    int yearIndex = 0;
    int monthIndex = 0;
    int deviceIndex=0;
    String selectYear="2021";
    private Toolbar toolbar=null;
    String sessionNumber;
    String deviceNumber;
    Boolean isSessionDeleted=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);
        initComponent();
        setToolbar();
        getIntentData();

        updateSession();
        spYear.setSelection(yearIndex);
        spMonth.setSelection(monthIndex);
        spDevice.setSelection(deviceIndex);
        setDeviceSpinner();
        addDeviceListener();
        setYearSpinner();
        addYearListener();
        setMonthSpinner();
        addMonthListener();
    }

    public void initComponent(){
        rvSession = findViewById(R.id.rvSession);
        spDevice=findViewById(R.id.spinnerDevice);
        spYear = findViewById(R.id.spinnerYear);
        spMonth = findViewById(R.id.spinnerMonth);
        sessionDAO = new SessionDAO(SessionActivity.this);
        sessionDetailDAO = new SessionDetailDAO(SessionActivity.this);
        toolbar=findViewById(R.id.toolbar);
    }

    public void getIntentData() {
        Intent intent = getIntent();
        sessionNumber = intent.getStringExtra(MainActivity3.SESSION_NUMBER);
        deviceNumber = intent.getStringExtra(MainActivity3.DEVICE_NUMBER);
    }

    public void setToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String tittle="Danh sách phiên";
        getSupportActionBar().setTitle(tittle);
        toolbar.setTitleTextColor(Color.parseColor("#2C5CA4"));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.session_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if(isSessionDeleted==true){
                sessionIsDelete();
            }
            else {
                finish();
                return true;
            }
        }
        if (id == R.id.action_add_session) {
                startActivity(new Intent(SessionActivity.this, LoginActivity.class));
                return true;
        }
        if(id==R.id.action_delete_all_session){
            deleteAllSession();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void sessionIsDelete(){
        AlertDialog.Builder builder = new AlertDialog.Builder(SessionActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_logout_session_screen, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SessionActivity.this,LoginActivity.class));
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void deleteAllSession() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SessionActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_deleteall_session, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionDAO.deleteAll();
                sessionDetailDAO.deleteAll();
                alertDialog.cancel();
                Toast.makeText(SessionActivity.this, "Xóa thành công tất cả phiên", Toast.LENGTH_SHORT).show();
                updateSession();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void setAdapter(){
        sessionAdapter = new SessionAdapter(SessionActivity.this, list, new SessionAdapter.ItemClickListener() {
            @Override
            public void onClick(Session session) {
                Intent intent = new Intent(SessionActivity.this, SessionDetailActivity.class);
                if(Integer.parseInt(session.getSessionNumber())<10){
                    intent.putExtra(MainActivity3.SESSION_NUMBER,"0"+session.getSessionNumber());
                }
                else {
                    intent.putExtra(MainActivity3.SESSION_NUMBER, session.getSessionNumber());
                }

                intent.putExtra(MainActivity3.DEVICE_NUMBER, session.getDeviceNumber());
                intent.putExtra(SessionDetailActivity.CREATE_DATE, simpleDateFormat.format(session.getCreateDate()));
                intent.putExtra(MainActivity3.SYNC_STATUS, session.getSyncStatus()+"");
                startActivity(intent);
            }
        }, new SessionAdapter.ItemDeleteListener() {
            @Override
            public void onClick(Session session) {
                if(session.getDeviceNumber().equalsIgnoreCase(deviceNumber)&&Integer.parseInt(session.getSessionNumber())
                        ==Integer.parseInt(sessionNumber)){
                    isSessionDeleted=true;

                }
                else
                    isSessionDeleted=false;
                dialogDeleteSession(session);
            }
        });
        rvSession.setLayoutManager(new LinearLayoutManager(this));
        rvSession.setAdapter(sessionAdapter);
        sessionAdapter.notifyDataSetChanged();
    }

    public void updateSession() {
        list = sessionDAO.getAll();
        setAdapter();
    }

    public void dialogDeleteSession(final Session session) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SessionActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_delete_session, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idSession=sessionDAO.getId(session.getSessionNumber(),session.getDeviceNumber());
                sessionDAO.delete(session);
                for (int i = 0; i < sessionDetailDAO.getBySession(idSession).size(); i++) {
                    sessionDetailDAO.delete(sessionDetailDAO.getBySession(idSession).get(i));
                }
                alertDialog.cancel();
                updateSession();
                Toast.makeText(SessionActivity.this, "Xóa thành công phiên " + session.getSessionNumber(), Toast.LENGTH_SHORT).show();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateSession();
    }

    private void setDeviceSpinner(){
        ArrayList<String> devices=new ArrayList<>();
        devices=sessionDAO.getListDevice();
        HashSet<String> hashSet = new HashSet<String>();
        hashSet.addAll(devices);
        devices.clear();
        devices.addAll(hashSet);
        devices.add(0,"All device");

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SessionActivity.this
                , android.R.layout.simple_spinner_item, devices);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDevice.setAdapter(spinnerAdapter);
    }

    private void addDeviceListener(){
        spDevice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if ((deviceIndex != position)) {
                    deviceIndex = position;
                }
                if(position==0){
                    list=sessionDAO.getAll();
                }
                else {
                    list = sessionDAO.getSessionByDevice(spDevice.getSelectedItem().toString());
                }
                setAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setYearSpinner() {
        curentYear = Calendar.getInstance().get(Calendar.YEAR);
        ArrayList<String> years = new ArrayList<>();
        for (int i = 2021; i <= curentYear; i++) {
            {
                years.add(String.valueOf(i));
            }
        }
        years.add(0,"All");


        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SessionActivity.this
                , android.R.layout.simple_spinner_item, years);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spYear.setAdapter(spinnerAdapter);
    }

    private void addYearListener() {
        spYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int position, long arg3) {
                selectYear=spYear.getSelectedItem().toString();
                if ((yearIndex != position)) {
                    yearIndex = position;
                }
                if(yearIndex==0){
                    list=sessionDAO.getAll();
                }
                else {
                    list = sessionDAO.getSessionByYear(yearIndex+2020);
                }
                setAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                updateSession();
            }
        });
    }

    private void setMonthSpinner() {
        ArrayList<String> months = new ArrayList<>();
        months.add(0,"All");
        for (int i = 1; i <= 12; i++) {
            if(i<10) {
                months.add("0" + i);
            }
            else
                months.add(i+"");
        }

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SessionActivity.this
                , android.R.layout.simple_spinner_item, months);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMonth.setAdapter(spinnerAdapter);
    }

    private void addMonthListener() {
        spMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int position, long arg3) {
                if ((monthIndex != position)) {
                    monthIndex = position;
                }
                if(monthIndex==0){
                    list=sessionDAO.getAll();
                }
                else {
                    list = sessionDAO.getSessionByYearMonth(selectYear,spMonth.getSelectedItem().toString());
                    if(selectYear.equalsIgnoreCase("All")){
                        list=sessionDAO.getSessionByMonth(spMonth.getSelectedItem().toString());
                    }
                }
                setAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                updateSession();
            }
        });
    }
}
