/*
 * Copyright (C) 2015-2019 Zebra Technologies Corporation and/or its affiliates
 * All rights reserved.
 */
package com.symbol.barcodesample1.UI;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.symbol.barcodesample1.Model.Book;
import com.symbol.barcodesample1.DAO.BookDAO;
import com.symbol.barcodesample1.Util.CSVWriter;
import com.symbol.barcodesample1.Util.ConnectServer;
import com.symbol.barcodesample1.Model.ExportBook;
import com.symbol.barcodesample1.R;
import com.symbol.barcodesample1.Model.Session;
import com.symbol.barcodesample1.DAO.SessionDAO;
import com.symbol.barcodesample1.Model.SessionDetail;
import com.symbol.barcodesample1.DAO.SessionDetailDAO;
import com.symbol.barcodesample1.Model.SyncStatus;
import com.symbol.barcodesample1.Adapter.BookAdapter;
import com.symbol.barcodesample1.Adapter.SessionDetailAdapter2;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.EMDKManager.EMDKListener;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.BarcodeManager.ScannerConnectionListener;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.DataListener;
import com.symbol.emdk.barcode.Scanner.StatusListener;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.StatusData;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity3 extends AppCompatActivity implements EMDKListener, DataListener, StatusListener, ScannerConnectionListener {

    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;

    private TextView textViewStatus = null;
    private TextView tvSessionNumber = null;
    private TextView tvSumQuantity = null;
    private Button btnStopScan = null;
    private EditText edtCurrentData = null;
    private Button btnExportAndSysn = null;
    private View vBottom = null;
    private Toolbar toolbar = null;
    private RecyclerView rvSessionDetail;
    private EditText edtScanQuantity = null;
    private Button btnSave = null;

    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;

    private String statusString = "";

    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;
    private final Object lock = new Object();

    public static final String SESSION_NUMBER = "session number";
    public static final String DEVICE_NUMBER = "device number";
    public static final String SYNC_STATUS = "sync status";
    public static final String ID_SESSION = "id session";
    String sessionNumber;
    String deviceNumber;
    int idSession;
    String ipAdress;
    Date createDate;
    SharedPreferences storage;

    ArrayList<Book> bookArrayList;
    BookDAO bookDAO;
    Book book;
    BookAdapter bookAdapter;
    HashMap<String, Integer> frequencymap;
    BookAdapter adapter;
    int sumSku = 0;
    ArrayList<String> skuList;
    SessionDetailDAO sessionDetailDAO;
    SessionDetail sessionDetail;
    ArrayList<SessionDetail> sessionDetailArrayList;
    SessionDetailAdapter2 sessionDetailAdapter2;
    SessionDAO sessionDAO;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    Boolean bSyncAnDelete = false;
    private SyncStatus syncStatus;
//    Boolean bInverse1Mode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        initComponent();
        getIntentData();
        getIdSession();
        setToolbar();
        getSetting();


        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }

        btnStopScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopScan();
            }
        });

        updateLv();

        btnExportAndSysn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bSyncAnDelete == false) {
                    exportAndSysnFile();
                    setSyncStatus();
                } else {
                    exportAndSysnFile();
                    setSyncStatus();
                    sessionDetailDAO.delete(idSession);
                    sessionDAO.delete(idSession);
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edtCurrentData.getText().toString())) {
                    Toast.makeText(MainActivity3.this, "Mã sku đang trống", Toast.LENGTH_SHORT).show();
                    edtCurrentData.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(edtScanQuantity.getText().toString())) {
                    Toast.makeText(MainActivity3.this, "Số lượng đang trống", Toast.LENGTH_SHORT).show();
                    edtScanQuantity.requestFocus();
                    return;
                }

                String sku = edtCurrentData.getText().toString();
                if (sessionDetailDAO.checkDuplicate(idSession, sku) == true) {
                    int quantity = Integer.parseInt(edtScanQuantity.getText().toString()) + sessionDetailDAO.getQuantityBySku(idSession, sku);
                    sessionDetail = new SessionDetail(idSession, sku, quantity);
                    addQuantityManualDuplicate(sessionDetail);

                    //hide keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    updateLv();
                } else {
                    int quantity = Integer.parseInt(edtScanQuantity.getText().toString());
                    sessionDetail = new SessionDetail(idSession, sku, quantity);
                    addQuantityManualNotDuplicate(sessionDetail);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    updateLv();
                }
            }
        });

        setSumSku();
        setSyncStatus();
    }

    public void initComponent() {
        btnSave = findViewById(R.id.btnSave);
        edtScanQuantity = findViewById(R.id.edtScanQuantity);
        rvSessionDetail = findViewById(R.id.rvSessionDetail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        vBottom = findViewById(R.id.vBottom);
        textViewStatus = (TextView) findViewById(R.id.textViewStatus);
        tvSessionNumber = findViewById(R.id.tvSessionNumber);
        tvSumQuantity = findViewById(R.id.tvSumQuantity);
        spinnerScannerDevices = (Spinner) findViewById(R.id.spinnerScannerDevices);
        btnStopScan = findViewById(R.id.btnStopScan);
        edtCurrentData = findViewById(R.id.edtCurerntData);
        btnExportAndSysn = findViewById(R.id.btnExportAndSysn);
        bookArrayList = new ArrayList<>();
        sessionDetailArrayList = new ArrayList<>();
        bookDAO = new BookDAO(MainActivity3.this);
        skuList = new ArrayList<>();
        sessionDetailDAO = new SessionDetailDAO(MainActivity3.this);
        frequencymap = new HashMap<String, Integer>();
        sessionDAO = new SessionDAO(MainActivity3.this);
        storage = getSharedPreferences(LoginActivity.PREFERENCES, Context.MODE_PRIVATE);
    }

    public void setSyncStatus() {
        textViewStatus.setText(syncStatus + "");
        if (sessionDAO.getSession(sessionNumber, deviceNumber).getSyncStatus() == SyncStatus.SYNCFAIL) {
            textViewStatus.setTextColor(Color.parseColor("#FF1517"));
        } else if (sessionDAO.getSession(sessionNumber, deviceNumber).getSyncStatus() == SyncStatus.NOTSYNC) {
            textViewStatus.setTextColor(Color.parseColor("#000000"));
        }
    }

    public void setSumSku() {
        sumSku = sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber);
        tvSumQuantity.setText("Tổng số lượng đã scan: " + sumSku);
        Log.d("sum", sumSku + "");
    }

    public void getSetting() {
//        if (String.valueOf(storage.getBoolean(SettingActivity.INVERSE1MODE, false)) != null) {
//            bInverse1Mode = storage.getBoolean(SettingActivity.INVERSE1MODE, false);
//        }
        if (String.valueOf(storage.getBoolean(SettingActivity.CONTINOUS_MODE, false)) != null) {
            bContinuousMode = storage.getBoolean(SettingActivity.CONTINOUS_MODE, false);
        }
        if (String.valueOf(storage.getBoolean(SettingActivity.SYNC_AND_DELETE, false)) != null) {
            bSyncAnDelete = storage.getBoolean(SettingActivity.SYNC_AND_DELETE, false);
        }
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String tittle = "MÁY " + deviceNumber + "_" + "PHIÊN " + sessionNumber;
        getSupportActionBar().setTitle(tittle);
        toolbar.setTitleTextColor(Color.parseColor("#2C5CA4"));
    }

    public void exportAndSysnFile() {
        if (isStoragePermissionGranted()) {
            String fileName = deviceNumber + sessionNumber;

            //
            ArrayList<ExportBook> exportBookArrayList = new ArrayList<>();
            for (int i = 0; i < sessionDetailArrayList.size(); i++) {
                String sku_quantity = sessionDetailArrayList.get(i).getSku() + "," + sessionDetailArrayList.get(i).getQuantity();
                ExportBook exportBook = new ExportBook(sku_quantity);
                exportBookArrayList.add(exportBook);
            }


            Boolean writeToCSV = CSVWriter
                    .generateCSV(new File(Environment.getExternalStorageDirectory()
                            , fileName + ".txt"), exportBookArrayList.toArray());
            if (writeToCSV == true) {
                Toast.makeText(MainActivity3.this, "Export to CSV Success !!!", Toast.LENGTH_SHORT).show();
            }
            if (storage.getString(LoginActivity.SERVER_IP, "") != null) {
                ipAdress = storage.getString(LoginActivity.SERVER_IP, "");
            }
            String result = ConnectServer.connect(ipAdress);
            if (result.equalsIgnoreCase("success to connect to server " + ipAdress)) {
                ConnectServer.sendFileToPC(ipAdress, fileName);
                syncStatus = SyncStatus.SYNCED;
                Session session = new Session();
                session = sessionDAO.getSession(sessionNumber, deviceNumber);
                session.setSyncStatus(syncStatus);
                sessionDAO.update(session);
                onButtonShowPopupWindowClick(vBottom);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                backToCreateSession();
            } else {
                onButtonShowPopupWindowClick2(vBottom);
                syncStatus = SyncStatus.SYNCFAIL;
                Session session = new Session();
                session = sessionDAO.getSession(sessionNumber, deviceNumber);
                session.setSyncStatus(syncStatus);
                sessionDAO.update(session);
            }
        }
    }

    public void onButtonShowPopupWindowClick2(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.bottomsheet_fail_connect, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });

        Button btnConnect = popupView.findViewById(R.id.btnConnect);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setting();
            }
        });
    }

    public void setting() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity3.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_setting, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
            }
        });

        final EditText edtIpAdress = alertDialog.findViewById(R.id.edtIpAdress);
        Button btnConnectServer = alertDialog.findViewById(R.id.btnConnectServer);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        final TextView tvConnectionResult = alertDialog.findViewById(R.id.tvConnectionResult);

        if (storage.getString(LoginActivity.SERVER_IP, "") != null) {
            edtIpAdress.setText(storage.getString(LoginActivity.SERVER_IP, ""));
        }

        btnConnectServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storage = getSharedPreferences(LoginActivity.PREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = storage.edit();
                editor.putString(LoginActivity.SERVER_IP, edtIpAdress.getText().toString());
                editor.commit();

                String result = ConnectServer.connect(edtIpAdress.getText().toString());
                tvConnectionResult.setText(result);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void onButtonShowPopupWindowClick(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.bottomsheet_success_connect, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }
    }

    public void getIntentData() {
        Intent intent = getIntent();
        sessionNumber = intent.getStringExtra(SESSION_NUMBER);
        deviceNumber = intent.getStringExtra(DEVICE_NUMBER);
        try {
            createDate = simpleDateFormat.parse(intent.getStringExtra(SessionDetailActivity.CREATE_DATE));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        syncStatus = SyncStatus.valueOf(intent.getStringExtra(MainActivity3.SYNC_STATUS));
    }

    public void getIdSession() {
        idSession = sessionDAO.getId(sessionNumber, deviceNumber);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
//       textViewStatus.setText("Status: " + "EMDK open success!");
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
            getSetting();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
//        updateStatus("EMDK closed unexpectedly! Please close and restart the application.");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanData> scanData = scanDataCollection.getScanData();
            for (ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                }
                else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
//                        updateStatus(e.getMessage());
                    }
                }
                break;
            case WAITING:
                statusString = "Nhấn nút scan hoặc phím cứng trên thiết bị để scan...";
//                updateStatus(statusString);
                break;
            case SCANNING:
                statusString = "Đang scan...";
//                updateStatus(statusString);
                break;
            case DISABLED:
                statusString = statusData.getFriendlyName() + " is disabled.";
//                updateStatus(statusString);
                break;
            case ERROR:
                statusString = "An error has occurred.";
//                updateStatus(statusString);
                break;
            default:
                break;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    bSoftTriggerSelected = false;
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
            status = scannerNameExtScanner + ":" + statusExtScanner;
//            updateStatus(status);
        } else {
            bExtScannerDisconnected = false;
            status = statusString + " " + scannerNameExtScanner + ":" + statusExtScanner;
//            updateStatus(status);
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
//                updateStatus("Failed to get the list of supported scanner devices! Please close and restart the application.");
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled=true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Reset continuous flag
            bContinuousMode = false;

            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
//                    updateStatus(e.getMessage());
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textViewStatus.setText("" + status);
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    updateSession(result);
                    //
                    final String sku = result;
                    int quantity = 1;
                    edtScanQuantity.setText("1");
                    Log.d("test", sessionDetailDAO.checkDuplicate(idSession, sku) + "");
//                    if(isShow==false){
//                        if (sessionDetailDAO.checkDuplicate(idSession, sku) == true) {
//                            int quantity1= sessionDetailDAO.getQuantityBySku(idSession,sku)+quantity;
//                            sessionDetail = new SessionDetail(idSession, sku, quantity1);
//                            int result=sessionDetailDAO.update(sessionDetail);
//                        }else {
//                            int quantity1= 1;
//                            sessionDetail = new SessionDetail(idSession, sku, quantity1);
//                            long result=sessionDetailDAO.insert(sessionDetail);
//                        }
//                    }
//                    if (sessionDetailDAO.checkDuplicate(idSession, sku) == true) {
//                        quantity = sessionDetailDAO.getQuantityBySku(idSession, sku);
//                        sessionDetail = new SessionDetail(idSession, sku, quantity);
//                        addQuantityDuplicateSku(sessionDetail);
//                        updateLv();
//                    } else {
//                        book = new Book(sku);
//                        sessionDetail = new SessionDetail(idSession, sku, quantity);
//                        bookDAO.insert(book);
//                        addQuantityNotDuplicateSku(sessionDetail);
//                        updateLv();
//                    }

                    if (bContinuousMode == true) {

                        if (sessionDetailDAO.checkDuplicate(idSession, sku) == true) {

                            int quantity1 = 1 + sessionDetailDAO.getQuantityBySku(idSession, sku);
                            sessionDetail = new SessionDetail(idSession, sku, quantity1);
                            addQuantityAutoDuplicate(sessionDetail);
                            updateLv();

                        } else {
                            int quantity1 = 1;
                            sessionDetail = new SessionDetail(idSession, sku, quantity1);
                            addQuantityAutoNotDuplicate(sessionDetail);
                            updateLv();

                        }
                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity3.this);
                        ViewGroup viewGroup = findViewById(android.R.id.content);
                        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_continue_scan, viewGroup, false);
                        builder.setView(dialogView);
                        final AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                        Button btnYes, btnNo;
                        btnYes = alertDialog.findViewById(R.id.btnYes);
                        btnNo = alertDialog.findViewById(R.id.btnNo);
                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (sessionDetailDAO.checkDuplicate(idSession, sku) == true) {
                                    int quantity1 = 1 + sessionDetailDAO.getQuantityBySku(idSession, sku);
                                    sessionDetail = new SessionDetail(idSession, sku, quantity1);
                                    addQuantityAutoDuplicate(sessionDetail);
                                    updateLv();
                                    alertDialog.dismiss();
                                } else {
                                    int quantity1 = 1;
                                    sessionDetail = new SessionDetail(idSession, sku, quantity1);
                                    addQuantityAutoNotDuplicate(sessionDetail);
                                    updateLv();
                                    alertDialog.dismiss();
                                }
                            }
                        });

                        btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.cancel();
                            }
                        });
                    }


                }
            }
        });
    }

    private void addQuantityAutoDuplicate(SessionDetail sessionDetail) {
        int result = sessionDetailDAO.update(sessionDetail);
        if (result > 0) {
            sessionDAO.update(new Session(deviceNumber, sessionNumber
                    , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) + 1, createDate, syncStatus));
            setSumSku();
            View content = findViewById(android.R.id.content);
            Snackbar.make(content, "Success add", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void addQuantityAutoNotDuplicate(SessionDetail sessionDetail) {
        //add new book
        book = new Book(edtCurrentData.getText().toString());
        bookDAO.insert(book);
        //add session detail
        long result = sessionDetailDAO.insert(sessionDetail);
        if (result > 0) {
            sessionDAO.update(new Session(deviceNumber, sessionNumber
                    , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) + 1, createDate, syncStatus));
            setSumSku();
            View content = findViewById(android.R.id.content);
            Snackbar.make(content, "Success add", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void addQuantityManualDuplicate(SessionDetail sessionDetail) {
        int scanQuantity = Integer.parseInt(edtScanQuantity.getText().toString());
        int result = sessionDetailDAO.update(sessionDetail);
        if (result > 0) {
            sessionDAO.update(new Session(deviceNumber, sessionNumber
                    , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) + scanQuantity, createDate, syncStatus));
            setSumSku();
            View content = findViewById(android.R.id.content);
            Snackbar.make(content, "Success add", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void addQuantityManualNotDuplicate(SessionDetail sessionDetail) {
        int scanQuantity = Integer.parseInt(edtScanQuantity.getText().toString());
        //add new book
        book = new Book(edtCurrentData.getText().toString());
        bookDAO.insert(book);
        //add session detail
        long result = sessionDetailDAO.insert(sessionDetail);
        if (result > 0) {
            sessionDAO.update(new Session(deviceNumber, sessionNumber
                    , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) + scanQuantity, createDate, syncStatus));
            setSumSku();
            View content = findViewById(android.R.id.content);
            Snackbar.make(content, "Success add", Snackbar.LENGTH_SHORT).show();
        }
    }

    public void updateSession(String result) {
        //Số lượng scan
        //sumSku = sumSku + 1;
        edtCurrentData.setText(result);
        tvSumQuantity.setText("Tổng số lượng đã scan: " + sumSku);
        Session session = new Session();
        session = new Session(deviceNumber, sessionNumber, sumSku, createDate, syncStatus);
        sessionDAO.update(session);
    }

//    public void addQuantityDuplicateAndContinueScan(SessionDetail sessionDetail1){
//        int quantity=1;
//        sessionDetail1.setSku(sessionDetail1.getSku());
//        sessionDetail1.setQuantity(sessionDetail1.getQuantity()+quantity);
//        sessionDetail1.setId(sessionDetail1.getId());
//        int result=sessionDetailDAO.update(sessionDetail1);
//        Log.d("result",result+"");
//        if(result>0) {
//            sessionDAO.update(new Session(deviceNumber, sessionNumber
//                    , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) + quantity, createDate, syncStatus));
//            setSumSku();
//            View content = findViewById(android.R.id.content);
//            Snackbar.make(content, "Success add", Snackbar.LENGTH_SHORT).show();
//        }
//    }
//
//    public void addQuantityNoDuplicateAndContinueScan(SessionDetail sessionDetail1){
//        int quantity=0;
//        sessionDetail1.setSku(sessionDetail1.getSku());
//        sessionDetail1.setQuantity(quantity++);
//        sessionDetail1.setId(sessionDetail1.getId());
//        long result=sessionDetailDAO.insert(sessionDetail1);
//        Log.d("result",result+"");
//        if(result>0){
//            sessionDAO.update(new Session(deviceNumber,sessionNumber
//                    ,sessionDAO.getSumSkuBySession(sessionNumber,deviceNumber)+quantity,createDate,syncStatus));
//            setSumSku();
//
//            View content = findViewById(android.R.id.content);
//            Snackbar.make(content, "Success add", Snackbar.LENGTH_SHORT).show();
//        }
//    }

    public void addQuantityDuplicateSku(final SessionDetail sessionDetail1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity3.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_add_quantity, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                updateLv();
            }
        });
        final TextView edtQuantity = alertDialog.findViewById(R.id.edtQuantity);
        final EditText edtScanQuantity = alertDialog.findViewById(R.id.edtScanQuantity);


        edtQuantity.setText(sessionDetailDAO.getQuantityBySku(idSession, sessionDetail1.getSku()) + "");
        edtScanQuantity.setText("1");
        TextView tvSku = alertDialog.findViewById(R.id.tvSku);
        tvSku.setText(sessionDetail1.getSku());
        Button btnSave = alertDialog.findViewById(R.id.btnSave);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
                    edtScanQuantity.requestFocus();
                    return;
                }
                int quantity = Integer.parseInt(edtScanQuantity.getText().toString());
                sessionDetail1.setSku(sessionDetail1.getSku());
                sessionDetail1.setQuantity(sessionDetail1.getQuantity() + quantity);
                sessionDetail1.setId(sessionDetail1.getId());
                int result = sessionDetailDAO.update(sessionDetail1);
                Log.d("result", result + "");
                if (result > 0) {

                    sessionDAO.update(new Session(deviceNumber, sessionNumber
                            , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) + quantity, createDate, syncStatus));
                    alertDialog.cancel();
                    setSumSku();
                    View content = findViewById(android.R.id.content);
                    Snackbar.make(content, "Success add", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void addQuantityNotDuplicateSku(final SessionDetail sessionDetail1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity3.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_add_quantity, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                updateLv();
            }
        });
        final TextView edtQuantity = alertDialog.findViewById(R.id.edtQuantity);
        final EditText edtScanQuantity = alertDialog.findViewById(R.id.edtScanQuantity);
        edtQuantity.setText("1");
        edtScanQuantity.setText("1");
        TextView tvSku = alertDialog.findViewById(R.id.tvSku);
        tvSku.setText(sessionDetail1.getSku());
        Button btnSave = alertDialog.findViewById(R.id.btnSave);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);

//        Handler handler=new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                alertDialog.cancel();
//            }
//        },2000);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
                    edtScanQuantity.requestFocus();
                    return;
                }
                int quantity = Integer.parseInt(edtScanQuantity.getText().toString());
                sessionDetail1.setSku(sessionDetail1.getSku());
                sessionDetail1.setQuantity(quantity);
                sessionDetail1.setId(sessionDetail1.getId());
                long result = sessionDetailDAO.insert(sessionDetail1);
                Log.d("result", result + "");
                if (result > 0) {
                    sessionDAO.update(new Session(deviceNumber, sessionNumber
                            , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) + quantity, createDate, syncStatus));
                    alertDialog.cancel();
                    setSumSku();

                    View content = findViewById(android.R.id.content);
                    Snackbar.make(content, "Success add", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void setAdapter() {
        sessionDetailAdapter2 = new SessionDetailAdapter2(MainActivity3.this, sessionDetailArrayList, new SessionDetailAdapter2.ItemClickListener() {
            @Override
            public void onClick(SessionDetail sessionDetail) {

            }
        }, new SessionDetailAdapter2.ItemSwipeListener() {
            @Override
            public void onSwipe(SessionDetail sessionDetail, boolean left, int index) {
                if (index == 0) {
                    editQuantityDialog(sessionDetail, sessionDetail.getQuantity());
                } else if (index == 1) {
                    deleteSessionDetailDialog(sessionDetail, sessionDetail.getQuantity());
                }
            }
        });
        rvSessionDetail.setLayoutManager(new LinearLayoutManager(MainActivity3.this));
        rvSessionDetail.setAdapter(sessionDetailAdapter2);
    }

    public void deleteSessionDetailDialog(final SessionDetail sessionDetail1, final int quantity1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity3.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_deleteall_session, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                updateLv();
            }
        });

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = sessionDetailDAO.delete(sessionDetail1);
                if (result > 0) {
                    sessionDAO.update(new Session(deviceNumber, sessionNumber
                            , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) - quantity1, createDate, syncStatus));
                    setSumSku();
                    alertDialog.cancel();
                    Toast.makeText(MainActivity3.this, "Xóa thành công mã " + sessionDetail1.getSku(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void editQuantityDialog(final SessionDetail sessionDetail1, final int quantity1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity3.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_edit_quantity, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                updateLv();
                setSumSku();
            }
        });
        final EditText edtQuantity = alertDialog.findViewById(R.id.edtQuantity);
        edtQuantity.setText(sessionDetail1.getQuantity() + "");
        Button btnSave = alertDialog.findViewById(R.id.btnSave);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
                    edtQuantity.requestFocus();
                    return;
                }
                int quantity = Integer.parseInt(edtQuantity.getText().toString());
                sessionDetail1.setSku(sessionDetail1.getSku());
                sessionDetail1.setQuantity(quantity);
                sessionDetail1.setId(sessionDetail1.getId());
                int result = sessionDetailDAO.update(sessionDetail1);
                if (result == 1) {
                    sessionDAO.update(new Session(deviceNumber, sessionNumber
                            , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) - quantity1 + quantity, createDate, syncStatus));
                    alertDialog.cancel();
                    View content = findViewById(android.R.id.content);
                    Snackbar.make(content, "Success edit", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void updateLv() {
        sessionDetailArrayList = sessionDetailDAO.getBySession(idSession);
        setAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            backToCreateSession();
        }
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity3.this, SettingActivity.class));
            return true;
        }
        if (id == R.id.action_list_session) {
            showListSession();
        }
        return super.onOptionsItemSelected(item);
    }

    public void showListSession() {
        Intent intent = new Intent(MainActivity3.this, SessionActivity.class);
        intent.putExtra(MainActivity3.DEVICE_NUMBER, deviceNumber);
        intent.putExtra(MainActivity3.SESSION_NUMBER, sessionNumber);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (true) {
            backToCreateSession();
        } else {
            super.onBackPressed();
            return;
        }

    }

    public void backToCreateSession() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity3.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_logout_scan_screen, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity3.this, LoginActivity.class);
                startActivity(intent);
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }
}
