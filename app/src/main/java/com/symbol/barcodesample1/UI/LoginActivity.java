package com.symbol.barcodesample1.UI;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.symbol.barcodesample1.Util.ConnectServer;
import com.symbol.barcodesample1.R;
import com.symbol.barcodesample1.Model.Session;
import com.symbol.barcodesample1.DAO.SessionDAO;
import com.symbol.barcodesample1.Model.SyncStatus;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LoginActivity extends AppCompatActivity {
    EditText edtDeviceNumber = null;
    EditText edtSessionNumber = null;
    ImageButton btnSetting = null;
    Button btnStart = null;
    Button btnListSession=null;
    TextView tvIP = null;
    ImageView ivConnectStatus = null;
    public static final String SERVER_IP = "server ip";
    public static final String PREFERENCES = "preferences";
    public static final String CURRENT_SESSION = "current session";
    SharedPreferences storage;
    SessionDAO sessionDAO;
    Session session;
    String ipAdress;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initComponent();

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSession();
            }
        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setting();
            }
        });

        btnListSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,SessionActivity.class));
            }
        });

        storage = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        if (storage.getString(MainActivity3.DEVICE_NUMBER, "") != null) {
            loadDeviceFromPreferences(edtDeviceNumber);
        }
        if (storage.getString(CURRENT_SESSION,"") != null) {
            loadCurrentSessionFromPreferences(edtSessionNumber);
        }

    }

    public void initComponent() {
        edtDeviceNumber = findViewById(R.id.edtDeviceNumber);
        edtSessionNumber = findViewById(R.id.edtSessionNumber);
        btnStart = findViewById(R.id.btnStart);
        btnSetting = findViewById(R.id.btnSetting);
        tvIP = findViewById(R.id.tvIP);
        ivConnectStatus = findViewById(R.id.ivConnectStatus);
        btnListSession=findViewById(R.id.btnListSession);
        sessionDAO = new SessionDAO(LoginActivity.this);
    }

    public void startSession() {
        try {
            final String sessionNumber = edtSessionNumber.getText().toString();
            final String deviceNumber = edtDeviceNumber.getText().toString();
            if (TextUtils.isEmpty(String.valueOf(sessionNumber))) {
                edtSessionNumber.requestFocus();
                edtSessionNumber.setError("Xin hãy nhập số phiên");
                return;
            }
            if (TextUtils.isEmpty(deviceNumber)) {
                edtDeviceNumber.requestFocus();
                edtSessionNumber.setError("Xin hãy nhập số máy");
                return;
            }



            if (sessionDAO.checkDuplicateSession(sessionNumber, deviceNumber) == true) {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                ViewGroup viewGroup = findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_continue_session, viewGroup, false);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();

                Button btnYes, btnNo;
                btnYes = alertDialog.findViewById(R.id.btnYes);
                btnNo = alertDialog.findViewById(R.id.btnNo);
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        continueExistSession(sessionNumber,deviceNumber);
                    }
                });
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.cancel();
                    }
                });

            } else {
                creatNewSession(sessionNumber,deviceNumber);
            }
        } catch (NumberFormatException ex) {
        }
    }

    public void creatNewSession(String sessionN,String deviceN){

        session = new Session();
        session.setSessionNumber(sessionN);
        session.setDeviceNumber(deviceN);
        session.setSumSku(0);
        sessionDAO.insert(session);
        Intent intent = new Intent(LoginActivity.this, MainActivity3.class);
        intent.putExtra(MainActivity3.SESSION_NUMBER, sessionN);
        intent.putExtra(MainActivity3.DEVICE_NUMBER, deviceN);
        intent.putExtra(SessionDetailActivity.CREATE_DATE,
                simpleDateFormat.format(Calendar.getInstance().getTime()));
        intent.putExtra(MainActivity3.SYNC_STATUS, SyncStatus.NOTSYNC+"");
        startActivity(intent);
        saveDeviceToPreferences(deviceN);
        saveCurrentSessionToPreferences(sessionN);
    }

    public void continueExistSession(String sessionN,String deviceN){
        Intent intent = new Intent(LoginActivity.this, MainActivity3.class);
        intent.putExtra(MainActivity3.SESSION_NUMBER, sessionN);
        intent.putExtra(MainActivity3.DEVICE_NUMBER, deviceN);
        intent.putExtra(SessionDetailActivity.CREATE_DATE,
                simpleDateFormat.format(Calendar.getInstance().getTime()));
        intent.putExtra(MainActivity3.SYNC_STATUS,sessionDAO.getSession(sessionN,deviceN).getSyncStatus()+"");
        startActivity(intent);
        saveDeviceToPreferences(deviceN);
        saveCurrentSessionToPreferences(sessionN);
    }

    public void setting() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_setting, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                connectServer();
            }
        });

        final EditText edtIpAdress = alertDialog.findViewById(R.id.edtIpAdress);
        Button btnConnectServer = alertDialog.findViewById(R.id.btnConnectServer);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        final TextView tvConnectionResult = alertDialog.findViewById(R.id.tvConnectionResult);

        if (storage.getString(SERVER_IP, "") != null) {
            loadFromPreferences(edtIpAdress);
        }

        btnConnectServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveToPreferences(edtIpAdress.getText().toString());
                String result = ConnectServer.connect(edtIpAdress.getText().toString());
                tvConnectionResult.setText(result);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void saveToPreferences(String ipAdress) {
        storage = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = storage.edit();
        editor.putString(SERVER_IP, ipAdress);
        editor.commit();
    }

    public void saveDeviceToPreferences(String deviceNumber) {
        storage = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = storage.edit();
        editor.putString(MainActivity3.DEVICE_NUMBER, deviceNumber);
        editor.commit();
    }

    public void saveCurrentSessionToPreferences(String sessionNumber) {
        storage = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = storage.edit();
        editor.putString(CURRENT_SESSION, sessionNumber);
        editor.commit();
    }


    public void loadFromPreferences(EditText editText) {
        editText.setText(storage.getString(SERVER_IP, ""));
    }

    public void loadFromPreferences(TextView textView) {
        textView.setText(storage.getString(SERVER_IP, ""));
    }

    public void loadDeviceFromPreferences(EditText editText) {
        editText.setText(storage.getString(MainActivity3.DEVICE_NUMBER, ""));
    }

    public void loadCurrentSessionFromPreferences(EditText editText) {
        String sessionNumber = storage.getString(CURRENT_SESSION, "");
        editText.setText(sessionNumber);
    }

    public void connectServer() {
        storage = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        if (storage.getString(SERVER_IP, "") != null) {
            loadFromPreferences(tvIP);
        }
        String result = ConnectServer.connect(tvIP.getText().toString());
        if (result.equalsIgnoreCase("success to connect to server " + tvIP.getText().toString())) {
            ivConnectStatus.setImageResource(R.drawable.ic_baseline_network_check_green);
        } else
            ivConnectStatus.setImageResource(R.drawable.ic_baseline_network_red);
    }

    @Override
    protected void onResume() {
        super.onResume();
        connectServer();
    }

}
