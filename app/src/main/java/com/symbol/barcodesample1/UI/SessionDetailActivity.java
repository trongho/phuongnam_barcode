package com.symbol.barcodesample1.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.symbol.barcodesample1.R;
import com.symbol.barcodesample1.Model.Session;
import com.symbol.barcodesample1.DAO.SessionDAO;
import com.symbol.barcodesample1.Model.SessionDetail;
import com.symbol.barcodesample1.DAO.SessionDetailDAO;
import com.symbol.barcodesample1.Model.SyncStatus;
import com.symbol.barcodesample1.Adapter.SessionDetailAdapter;
import com.symbol.barcodesample1.Adapter.SessionDetailAdapter2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SessionDetailActivity extends AppCompatActivity {
    private RecyclerView rvSessionDetail = null;
    private Toolbar toolbar = null;
    String sessionNumber;
    String deviceNumber;
    SyncStatus syncStatus;
    Date createDate;
    int idSession;
    String creatDate;
    TextView tvNoData;
    private Button btnDeleteAllBook = null;
    private Button btnContinueScan = null;
    private int sortIndex = 0;
    private Spinner spSort;
    SessionDetail sessionDetail;
    SessionDetailDAO sessionDetailDAO;
    SessionDAO sessionDAO;
    ArrayList<SessionDetail> sessionDetailArrayList;
    SessionDetailAdapter sessionDetailAdapter;
    SessionDetailAdapter2 sessionDetailAdapter2;
    public static final String CREATE_DATE = "create date";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_detail);
        initComponent();
        getIntentData();
        getIdSession();
        setToolbar();
        updateLv();

        btnDeleteAllBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionDetailDAO.delete(idSession);
                Session session = null;
                try {
                    session = new Session(deviceNumber, sessionNumber, 0
                            , simpleDateFormat.parse(creatDate), syncStatus);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                sessionDAO.update(session);
                Toast.makeText(SessionDetailActivity.this
                        , "Success delete data session " + sessionNumber, Toast.LENGTH_SHORT).show();
                updateLv();
            }
        });

        btnContinueScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SessionDetailActivity.this, MainActivity3.class);
                intent.putExtra(MainActivity3.SESSION_NUMBER, sessionNumber);
                intent.putExtra(MainActivity3.DEVICE_NUMBER, deviceNumber);
                intent.putExtra(SessionDetailActivity.CREATE_DATE, creatDate);
                intent.putExtra(MainActivity3.SYNC_STATUS, syncStatus + "");
                startActivity(intent);
            }
        });


        setSortSpinner();
        addSpSortListener();
    }

    public void initComponent() {
        tvNoData = findViewById(R.id.tvNoData);
        btnDeleteAllBook = findViewById(R.id.btnDeleteAllBook);
        btnContinueScan = findViewById(R.id.btnContinueScan);
        rvSessionDetail = findViewById(R.id.rvSessionDetail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        spSort = findViewById(R.id.spinnerSort);
        sessionDetailDAO = new SessionDetailDAO(SessionDetailActivity.this);
        sessionDAO = new SessionDAO(SessionDetailActivity.this);
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String tittle = "MÁY " + deviceNumber + "_" + "PHIÊN " + sessionNumber;
        getSupportActionBar().setTitle(tittle);
        toolbar.setTitleTextColor(Color.parseColor("#2C5CA4"));
    }

    public void getIdSession() {
        idSession = sessionDAO.getId(sessionNumber, deviceNumber);
    }

    public void getIntentData() {
        Intent intent = getIntent();
        sessionNumber = intent.getStringExtra(MainActivity3.SESSION_NUMBER);
        deviceNumber = intent.getStringExtra(MainActivity3.DEVICE_NUMBER);
        try {
            createDate = simpleDateFormat.parse(intent.getStringExtra(SessionDetailActivity.CREATE_DATE));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        syncStatus = SyncStatus.valueOf(intent.getStringExtra(MainActivity3.SYNC_STATUS));
        creatDate = intent.getStringExtra(SessionDetailActivity.CREATE_DATE);
    }

    public void setAdapter() {
        sessionDetailAdapter2 = new SessionDetailAdapter2(SessionDetailActivity.this, sessionDetailArrayList, new SessionDetailAdapter2.ItemClickListener() {
            @Override
            public void onClick(SessionDetail sessionDetail) {

            }
        }, new SessionDetailAdapter2.ItemSwipeListener() {
            @Override
            public void onSwipe(SessionDetail sessionDetail, boolean left, int index) {
                if (index == 0) {
                    editQuantityDialog(sessionDetail, sessionDetail.getQuantity());
                } else if (index == 1) {
                    deleteSessionDetailDialog(sessionDetail, sessionDetail.getQuantity());
                }
            }
        });
        rvSessionDetail.setLayoutManager(new LinearLayoutManager(SessionDetailActivity.this));
        rvSessionDetail.setAdapter(sessionDetailAdapter2);
        if (sessionDetailArrayList.size() == 0) {
            tvNoData.setText("Chưa có dữ liệu cho phiên này");
        }
    }

    public void deleteSessionDetailDialog(final SessionDetail sessionDetail1, final int quantity1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SessionDetailActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_deleteall_session, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                updateLv();
            }
        });

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = sessionDetailDAO.delete(sessionDetail1);
                if (result > 0) {
                    alertDialog.cancel();
                    sessionDAO.update(new Session(deviceNumber, sessionNumber
                            , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) - quantity1, createDate, syncStatus));
                    Toast.makeText(SessionDetailActivity.this, "Xóa thành công mã " + sessionDetail1.getSku(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void editQuantityDialog(final SessionDetail sessionDetail1, final int quantity1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SessionDetailActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_edit_quantity, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                updateLv();
            }
        });
        final EditText edtQuantity = alertDialog.findViewById(R.id.edtQuantity);
        edtQuantity.setText(sessionDetail1.getQuantity() + "");
        Button btnSave = alertDialog.findViewById(R.id.btnSave);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
                    edtQuantity.requestFocus();
                    return;
                }
                int quantity = Integer.parseInt(edtQuantity.getText().toString());
                sessionDetail1.setSku(sessionDetail1.getSku());
                sessionDetail1.setQuantity(quantity);
                sessionDetail1.setId(sessionDetail1.getId());
                int result = sessionDetailDAO.update(sessionDetail1);
                if (result > 0) {
                    sessionDAO.update(new Session(deviceNumber, sessionNumber
                            , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) - quantity1 + quantity, createDate, syncStatus));
                    alertDialog.cancel();
                    View content = findViewById(android.R.id.content);
                    Snackbar.make(content, "Success edit", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void updateLv() {
        sessionDetailArrayList = sessionDetailDAO.getBySession(idSession);
        setAdapter();
    }

    public void sortAscending() {
        sessionDetailArrayList = sessionDetailDAO.getSortAscending(idSession);
        setAdapter();
    }

    public void sortDescending() {
        sessionDetailArrayList = sessionDetailDAO.getSortDescending(idSession);
        setAdapter();
    }

    public void sortABC() {
        sessionDetailArrayList = sessionDetailDAO.getSortABC(idSession);
        setAdapter();
    }

    private void setSortSpinner() {
        ArrayList<String> sortOptions = new ArrayList<>();
        sortOptions.add("Sort by a-z");
        sortOptions.add("Sort ascending");
        sortOptions.add("Sort descending ");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SessionDetailActivity.this
                , android.R.layout.simple_spinner_item, sortOptions);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSort.setAdapter(spinnerAdapter);
    }

    private void addSpSortListener() {
        spSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int position, long arg3) {
                if ((sortIndex != position)) {
                    sortIndex = position;
                }
                switch (sortIndex) {
                    case 0:
                        sortABC();
                    case 1:
                        sortAscending();
                    case 2:
                        sortDescending();
                    default:
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.session_detail_menu, menu);
        final MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setQueryHint("Search sku");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
//                sessionDetailArrayList=sessionDetailDAO.getBySku(s);
//                myActionMenuItem.collapseActionView();
//                setAdapter();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
//                setAdapter();
//                sessionDetailAdapter2.getFilter().filter(s.toString());
                ArrayList<SessionDetail> searchlist = new ArrayList<>();
                if (s.length() == 0) {
                    searchlist = sessionDetailArrayList;
                } else {
                    for (SessionDetail sessionDetail : sessionDetailArrayList) {
                        if (sessionDetail.getSku().toLowerCase().contains(s.toString().toLowerCase())) {
                            searchlist.add(sessionDetail);
                        }
                    }
                }
                sessionDetailAdapter2 = new SessionDetailAdapter2(SessionDetailActivity.this, searchlist, new SessionDetailAdapter2.ItemClickListener() {
                    @Override
                    public void onClick(SessionDetail sessionDetail) {

                    }
                }, new SessionDetailAdapter2.ItemSwipeListener() {
                    @Override
                    public void onSwipe(SessionDetail sessionDetail, boolean left, int index) {
                        if (index == 0) {
                            editQuantityDialog(sessionDetail, sessionDetail.getQuantity());
                        } else if (index == 1) {
                            deleteSessionDetailDialog(sessionDetail, sessionDetail.getQuantity());
                        }
                    }
                });
                rvSessionDetail.setLayoutManager(new LinearLayoutManager(SessionDetailActivity.this));
                rvSessionDetail.setAdapter(sessionDetailAdapter2);
                if (sessionDetailArrayList.size() == 0) {
                    tvNoData.setText("Chưa có dữ liệu cho phiên này");
                }
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        if (id == R.id.action_search) {

        }
        return super.onOptionsItemSelected(item);
    }

}