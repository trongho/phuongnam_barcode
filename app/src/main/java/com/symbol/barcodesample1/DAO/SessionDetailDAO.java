package com.symbol.barcodesample1.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ParseException;

import com.symbol.barcodesample1.Util.DBHelper;
import com.symbol.barcodesample1.Model.SessionDetail;

import java.util.ArrayList;

public class SessionDetailDAO {
    DBHelper dbHelper;
    public SessionDetailDAO(Context context){
        dbHelper=new DBHelper(context);
    }

    public ArrayList<SessionDetail> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int sessionNumber = c.getInt(0);
                String sku = c.getString(1);
                int quantity=c.getInt(2);
                list.add(new SessionDetail(sessionNumber,sku,quantity));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<SessionDetail> getSortAscending(int idSession) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? ORDER BY QUANTITY ASC";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(idSession)});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int sessionN = c.getInt(0);
                String sku = c.getString(1);
                int quantity=c.getInt(2);
                list.add(new SessionDetail(sessionN,sku,quantity));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<SessionDetail> getSortABC(int idSession) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? ORDER BY SKU ASC";
        Cursor c = db.rawQuery(sql,new String[]{String.valueOf(idSession)});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int sessionN = c.getInt(0);
                String sku = c.getString(1);
                int quantity=c.getInt(2);
                list.add(new SessionDetail(sessionN,sku,quantity));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<SessionDetail> getSortDescending(int idSession) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? ORDER BY QUANTITY DESC";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(idSession)});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int sessionN = c.getInt(0);
                String sku = c.getString(1);
                int quantity=c.getInt(2);
                list.add(new SessionDetail(sessionN,sku,quantity));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<SessionDetail> getBySession(int idSession) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? ";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(idSession)});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int id=c.getInt(0);
                String sku = c.getString(1);
                int quantity=c.getInt(2);
                list.add(new SessionDetail(id,sku,quantity));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<SessionDetail> getBySku(String sku1){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE SKU=? ";
        Cursor c = db.rawQuery(sql, new String[]{sku1});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int id=c.getInt(0);
                String sku = c.getString(1);
                int quantity=c.getInt(2);
                list.add(new SessionDetail(id,sku,quantity));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public Boolean checkDuplicate(int id,String sku) {
        Boolean flag = false;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? AND SKU=?";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(id),sku});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (c != null && c.getCount() > 0) {
                flag = true;
            }
            c.moveToNext();
        }
        return flag;
    }

    public int getQuantityBySku(int id,String sku){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        int quantity=0;
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? AND SKU=?";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(id),sku});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (c != null && c.getCount() > 0) {
                quantity=c.getInt(2);
            }
            c.moveToNext();
        }
        return quantity;
    }


    public long insert(SessionDetail sessionDetail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("ID_SESSION", sessionDetail.getId());
        values.put("SKU", sessionDetail.getSku());
        values.put("QUANTITY", sessionDetail.getQuantity());
        return db.insert("SESSION_DETAIL", null, values);
    }

    public int update(SessionDetail sessionDetail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("ID_SESSION", sessionDetail.getId());
        values.put("SKU", sessionDetail.getSku());
        values.put("QUANTITY", sessionDetail.getQuantity());
        return db.update("SESSION_DETAIL", values, "SKU=? AND ID_SESSION=?", new String[]{sessionDetail.getSku(), String.valueOf(sessionDetail.getId())});
    }

    public int delete(SessionDetail sessionDetail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("SESSION_DETAIL", "ID_SESSION=? AND SKU=?", new String[]{String.valueOf(sessionDetail.getId()),sessionDetail.getSku()});
    }

    public int delete(int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("SESSION_DETAIL", "ID_SESSION=?", new String[]{String.valueOf(id)});
    }

    public int deleteAll() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("SESSION_DETAIL",null,null);
    }
}
