package com.symbol.barcodesample1.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ParseException;

import com.symbol.barcodesample1.Util.DBHelper;
import com.symbol.barcodesample1.Model.Book;

import java.util.ArrayList;

public class BookDAO {
    DBHelper dbHelper;

    public BookDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<Book> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Book> list = new ArrayList<>();
        String sql = "SELECT * FROM BOOK";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String sku = c.getString(0);
                list.add(new Book(sku));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public long insert(Book book) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("SKU", book.getSku());
        return db.insert("BOOK", null, values);
    }

    public int update(Book book) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("SKU", book.getSku());
        return db.update("BOOK", values, "SKU=?", new String[]{book.getSku()});
    }

    public int delete(Book book) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("BOOK", "SKU=?", new String[]{book.getSku()});
    }

    public int deleteAll() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("BOOK", null, null);
    }
}
