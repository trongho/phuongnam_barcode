package com.symbol.barcodesample1.Model;

import java.util.Date;

public class Session {
    private String deviceNumber;
    private String sessionNumber;
    private int sumSku;
    private Date createDate;
    private SyncStatus syncStatus;

    public Session(String deviceNumber, String sessionNumber,int sumSku,Date createDate,SyncStatus syncStatus) {
        this.deviceNumber = deviceNumber;
        this.sessionNumber = sessionNumber;
        this.sumSku=sumSku;
        this.createDate=createDate;
        this.syncStatus=syncStatus;
    }

    public Session() {
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getSessionNumber() {
        return sessionNumber;
    }

    public void setSessionNumber(String sessionNumber) {
        this.sessionNumber = sessionNumber;
    }

    public int getSumSku() {
        return sumSku;
    }

    public void setSumSku(int sumSku) {
        this.sumSku = sumSku;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public SyncStatus getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(SyncStatus syncStatus) {
        this.syncStatus = syncStatus;
    }
}
