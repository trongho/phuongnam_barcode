package com.symbol.barcodesample1.Model;

public class ExportBook {
    private String sku_quantity;

    public ExportBook(String sku_quantity) {
        this.sku_quantity = sku_quantity;
    }

    public ExportBook() {
    }

    public String getSku_quantity() {
        return sku_quantity;
    }

    public void setSku_quantity(String sku_quantity) {
        this.sku_quantity = sku_quantity;
    }
}
