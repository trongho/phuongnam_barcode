package com.symbol.barcodesample1.Model;

public class SessionDetail {
    private int id;
    private String sku;
    private int quantity;

    public SessionDetail(int id, String sku,int quantity) {
        this.id = id;
        this.sku = sku;
        this.quantity=quantity;
    }

    public SessionDetail() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
