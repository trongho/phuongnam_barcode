package com.symbol.barcodesample1.Model;

public class Book {
    private String sku;


    public Book(String sku) {
        this.sku = sku;
    }

    public Book() {
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }
}
