package com.symbol.barcodesample1.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alexandrius.accordionswipelayout.library.SwipeLayout;
import com.symbol.barcodesample1.R;
import com.symbol.barcodesample1.Model.SessionDetail;

import java.util.ArrayList;

public class SessionDetailAdapter extends BaseAdapter implements Filterable {
    private Context context; //context
    private ArrayList<SessionDetail> sessionDetails; //data source of the list adapter
    private ArrayList<SessionDetail> origsessionDetails;
    ItemClickListener itemClickListener;
    private Filter sessionDetailFilter;

    public SessionDetailAdapter(Context context, ArrayList<SessionDetail> sessionDetails,ItemClickListener itemClickListener){
        this.context=context;
        this.sessionDetails=sessionDetails;
        this.itemClickListener=itemClickListener;
        this.origsessionDetails=sessionDetails;
    }
    @Override
    public int getCount() {
        if(sessionDetails!=null){
            return sessionDetails.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return sessionDetails.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.swipe_layout, viewGroup, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        SessionDetail sessionDetail=sessionDetails.get(position);
        viewHolder.itemSku.setText(sessionDetail.getSku());
        viewHolder.itemQuantity.setText(sessionDetail.getQuantity()+"");

        viewHolder.rlSessionDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemClickListener != null) {
                    itemClickListener.onClick(sessionDetails.get(position));
                }
            }
        });

        if(position%2==0){
            viewHolder.rlSessionDetail.setBackgroundColor(Color.parseColor("#CDE0F1"));
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if(sessionDetailFilter==null){
            sessionDetailFilter=new SessionDetailFilter();
        }
        return sessionDetailFilter;
    }

    private class ViewHolder {
        TextView itemSku;
        TextView itemQuantity;
        RelativeLayout rlSessionDetail;
        SwipeLayout swipeLayout;

        public ViewHolder(View view) {
            itemSku = (TextView)view.findViewById(R.id.tvSku);
            itemQuantity = (TextView) view.findViewById(R.id.tvQuantity);
            rlSessionDetail=view.findViewById(R.id.rlSessionDetail);
            swipeLayout=view.findViewById(R.id.swipe_layout);
        }
    }

    public interface ItemClickListener {
        void onClick(SessionDetail sessionDetail);
    }

    public void resetData() {
        sessionDetails = origsessionDetails;
    }

    private class SessionDetailFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (charSequence == null || charSequence.length() == 0) {
                // No filter implemented we return all the list
                results.values = origsessionDetails;
                results.count = origsessionDetails.size();
            }
            else {
                // We perform filtering operation
                ArrayList<SessionDetail> sessionDetails = new ArrayList<SessionDetail>();

                for (SessionDetail sessionDetail : sessionDetails) {
                    if (sessionDetail.getSku().toUpperCase().startsWith(charSequence.toString().toUpperCase()))
                        sessionDetails.add(sessionDetail);
                }

                results.values = sessionDetails;
                results.count = sessionDetails.size();

            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            // Now we have to inform the adapter about the new list filtered
            sessionDetails= (ArrayList<SessionDetail>) filterResults.values;
            notifyDataSetChanged();
        }
    }
}


