package com.symbol.barcodesample1.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.symbol.barcodesample1.R;
import com.symbol.barcodesample1.Model.Session;
import com.symbol.barcodesample1.Model.SyncStatus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class SessionAdapter extends RecyclerView.Adapter<SessionAdapter.ViewHolder>  {
    Context context;
    ArrayList<Session> list;
    Session session;
    ItemClickListener listener;
    ItemDeleteListener deleteListener;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public SessionAdapter(Context context, ArrayList<Session> list, ItemClickListener listener,ItemDeleteListener deleteListener) {
        this.context = context;
        this.list = list;
        this.listener=listener;
        this.deleteListener=deleteListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.item_session, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        session=list.get(i);
        if(Integer.parseInt(session.getSessionNumber())<10){
            viewHolder.tvSessionNumber.setText("0"+session.getSessionNumber());
        }
        else {
            viewHolder.tvSessionNumber.setText(session.getSessionNumber());
        }

        viewHolder.tvDeviceNumber.setText(session.getDeviceNumber());
        viewHolder.tvSumQuantity.setText(session.getSumSku()+"");
        viewHolder.tvCreateDate.setText(simpleDateFormat.format(session.getCreateDate()));
        viewHolder.llSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });
        viewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (deleteListener != null) {
                    deleteListener.onClick(list.get(i));
                }
            }
        });

        if(session.getSyncStatus()== SyncStatus.SYNCFAIL){
            viewHolder.llSession.setBackgroundColor(Color.parseColor("#FFB2A6"));
        }
        if(session.getSyncStatus()==SyncStatus.SYNCED){
            viewHolder.llSession.setBackgroundColor(Color.parseColor("#B0FF89"));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSessionNumber,tvDeviceNumber,tvSumQuantity,tvCreateDate;
        public LinearLayout llSession;
        public ImageView ivDelete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSessionNumber=itemView.findViewById(R.id.tvSessionNumber);
            tvDeviceNumber=itemView.findViewById(R.id.tvDeviceNumber);
            tvSumQuantity=itemView.findViewById(R.id.tvSumQuantity);
            tvCreateDate=itemView.findViewById(R.id.tvCreateDate);
            llSession=itemView.findViewById(R.id.llSession);
            ivDelete=itemView.findViewById(R.id.ivDelete);
        }
    }

    public void updateList(List<Session> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(Session session);
    }

    public interface ItemDeleteListener {
        void onClick(Session session);
    }
}
