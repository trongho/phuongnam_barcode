package com.symbol.barcodesample1.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.symbol.barcodesample1.Model.Book;
import com.symbol.barcodesample1.R;

import java.util.ArrayList;

public class BookAdapter extends BaseAdapter {
    private Context context; //context
    private ArrayList<Book> books; //data source of the list adapter

    public BookAdapter(Context context,ArrayList<Book> books){
        this.context=context;
        this.books=books;
    }
    @Override
    public int getCount() {
        return books.size();
    }

    @Override
    public Object getItem(int i) {
        return books.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_book, viewGroup, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Book book=books.get(position);
        viewHolder.itemSku.setText(book.getSku());

        return convertView;
    }

    private class ViewHolder {
        TextView itemSku;

        public ViewHolder(View view) {
            itemSku = (TextView)view.findViewById(R.id.tvSku);
        }
    }
}


